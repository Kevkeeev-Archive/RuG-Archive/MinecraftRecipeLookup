# MinecraftRecipeLookup
Minecraft Recipe Lookup program

This is a program that was created as part of an assignment for a course in OOP, roughly a year and a half back.

I merged two versions together and have no idea whatsoever how well it works. (We did "version control" in dropbox, back then.)

The other developer was https://github.com/GreenpantsDeveloper but as far as I can see he doesn't have a public repository of this code so I uploaded it.


I am currently using it as example code for my other two projects, PhotoSelectorSys and InventoryManglement.
