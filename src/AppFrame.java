import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.NoSuchElementException;
import java.util.Observable;
import java.util.Observer;
import java.util.Stack;
import java.util.StringTokenizer;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.border.BevelBorder;
import javax.swing.filechooser.FileNameExtensionFilter;


@SuppressWarnings("serial")
public class AppFrame extends JFrame implements Observer {
	
	private Database data;
	private String filePath;
	private ItemPanel itemPanel;
	private CraftingPanel craftingPanel;
	private JLabel statusBar;
	private JMenuItem toggleObtainableItem;
	private Item selectedItem;
	private Stack<Item> nextSequence;
	// private Stack<Item> previousSequence;
	
	public AppFrame() {
		super("Minecraft Recipes");
		filePath = null;
		setSize(1000, 610);
		setLocationRelativeTo(null);
		createMenuBar();
		createStatusBar();
		data = new Database();
		selectedItem = null;
		nextSequence = new Stack<>();
		// previousSequence = new Stack<>();
		
		craftingPanel = new CraftingPanel(750, 600);
		craftingPanel.getCraftingInfoPanel().getRecipePanel().addListener(this);
		craftingPanel.getCraftingInfoPanel().getRequirementsPanel().addListener(this);
		itemPanel = new ItemPanel(data.getItems(), 250, 493);
		itemPanel.addListener(this);
		add(itemPanel, BorderLayout.WEST);
		add(craftingPanel, BorderLayout.EAST);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setResizable(false);
		setVisible(true);
		data.addObserver(this);
	}
	
	private void createMenuBar(){
		// Initialising the menubar
		JMenuBar menuBar = new JMenuBar();
		JMenu file = new JMenu("File");
		JMenu edit = new JMenu("Edit");
		JMenu view = new JMenu("View");
		JMenu help = new JMenu("Help");
		JMenuItem newItem = new JMenuItem("New");
		JMenuItem openItem = new JMenuItem("Open...");
		JMenuItem saveItem = new JMenuItem("Save");
		JMenuItem saveAsItem = new JMenuItem("Save As...");
		JMenuItem exitItem = new JMenuItem("Exit");
		JMenuItem addItemItem = new JMenuItem("Add Item");
		JMenuItem addRecipeItem = new JMenuItem("Add Recipe");
		JMenuItem addCraftingItem = new JMenuItem("Add Crafting Method");
		JMenuItem editRecipeItem = new JMenuItem("Edit Recipe");
		JMenuItem editCraftingItem = new JMenuItem("Edit Crafting Method");
		JMenuItem editItemItem = new JMenuItem("Edit Item");
		toggleObtainableItem = new JMenuItem("Include Unobtainable Items");
		JMenuItem refreshItem = new JMenuItem("Refresh");
		JMenuItem reloadItem = new JMenuItem("Reload");
		JMenuItem tipsItem = new JMenuItem("Tips");
		JMenuItem aboutItem = new JMenuItem("About");
		
		// Setting up actionlisteners for the buttons
		newItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent event) {
                menuNew();
            }
        });
		
		openItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent event) {
                menuOpen();
            }
        });
		
		saveItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent event) {
                menuSave();
            }
        });
		
		saveAsItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent event) {
                menuSaveAs();
            }
        });
		
		addRecipeItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent event) {
                menuAddRecipe();
            }
        });
		
		editRecipeItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent event) {
                menuEditRecipe();
            }
        });
		
		addItemItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent event) {
                menuAddItem();
            }
        });
		
		editItemItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent event) {
                menuEditItem();
            }
        });
		
		addCraftingItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent event) {
                menuAddCrafting();
            }
        });
		
		editCraftingItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent event) {
                menuEditCrafting();
            }
        });
		
		refreshItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent event) {
                refresh();
            }
        });
		
		reloadItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent event) {
                menuReload();
            }
        });
		
		exitItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent event) {
                System.exit(0);
            }
        });
		
		toggleObtainableItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent event) {
                menuToggleObtainable();
            }
        });
		
		tipsItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent event) {
                menuTips();
            }
        });
		
		aboutItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent event) {
                menuAbout();
            }
        });
		
		// Finalising the menubar
		file.add(newItem);
		file.add(openItem);
		file.addSeparator();
		file.add(saveItem);
		file.add(saveAsItem);
		file.addSeparator();
		file.add(exitItem);
		menuBar.add(file);
		edit.add(addItemItem);
		edit.add(addRecipeItem);
		edit.add(addCraftingItem);
		edit.addSeparator();
		edit.add(editItemItem);
		edit.add(editRecipeItem);
		edit.add(editCraftingItem);
		menuBar.add(edit);
		view.add(toggleObtainableItem);
		view.addSeparator();
		view.add(refreshItem);
		view.add(reloadItem);
		menuBar.add(view);
		help.add(tipsItem);
		help.add(aboutItem);
		menuBar.add(help);
		setJMenuBar(menuBar);
	}
	
	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}
	
	private void createStatusBar(){
		JPanel textPanel = new JPanel();
		textPanel.setBorder(new BevelBorder(BevelBorder.LOWERED));
		add(textPanel, BorderLayout.SOUTH);
		textPanel.setPreferredSize(new Dimension(getWidth(), 20));
		textPanel.setLayout(new BoxLayout(textPanel, BoxLayout.X_AXIS));
		statusBar = new JLabel("");
		statusBar.setHorizontalAlignment(SwingConstants.LEFT);
		textPanel.add(statusBar);
	}
	
	public void menuNew(){
		data.clearData();
		itemPanel.deselect();
	}
	
	private void menuOpen(){
		// Use the JFileChooser to find the path to a user defined file to load
		JFileChooser chooser = new JFileChooser();
	    FileNameExtensionFilter filter = new FileNameExtensionFilter(".tsv files", "tsv");
	    chooser.setFileFilter(filter);
	    int returnVal = chooser.showOpenDialog(null);
	    if(returnVal == JFileChooser.APPROVE_OPTION) {
	    	// Start with new, empty frame and then load the file
	    	menuNew();
	    	String path = chooser.getSelectedFile().getAbsolutePath();
	    	data.loadFile(path);
	    	filePath = path;
	    } else {
	    	say("Opening cancelled");
	    }
	}
	
	private void menuSave(){
		if(filePath==null){
			// If no file has been opened yet, "save" button acts as "save as" button
			menuSaveAs();
		} else {
			data.saveFile(filePath);
		}
	}
	
	private void menuSaveAs(){
		// Use the JFileChooser to find the path to a user defined file to load
		JFileChooser chooser = new JFileChooser();
	    FileNameExtensionFilter filter = new FileNameExtensionFilter(".tsv files", "tsv");
	    chooser.setFileFilter(filter);
	    int returnVal = chooser.showSaveDialog(null);
	    if(returnVal == JFileChooser.APPROVE_OPTION) {
	    	// Make sure the file will be properly saved with extension
	    	String path = chooser.getSelectedFile().getAbsolutePath();
	    	data.saveFile(path);
	    	filePath = path;
	    } else {
	    	say("Saving cancelled");
	    }
	}
	
	private void menuAddItem(){
		JTextField iDField = new JTextField();
		JTextField nameField = new JTextField();
		JCheckBoxMenuItem obtainableField = new JCheckBoxMenuItem();
		JTextField tagsField = new JTextField();
		Object[] options = {
		    "Item ID", iDField,
		    "Item name", nameField,
		    "Is the item obtainable in Survival mode?", obtainableField,
		    "[optional] search tags, separated by a semicolon (;)", tagsField,
		};
		
		// Set default values for improved user experience
		obtainableField.setSelected(true);
		iDField.setText(data.getNextAvailableID());
		
		int option = JOptionPane.showConfirmDialog(null, options, "Add Item...", JOptionPane.OK_CANCEL_OPTION);
		if (option == JOptionPane.OK_OPTION) {
		    String iD = iDField.getText();
		    String name = nameField.getText();
		    boolean obtainable = obtainableField.isSelected();
		    String potentialTags = tagsField.getText();
		    // Now make sure all fields are valid
		    
		    if(!iD.matches("\\d+(:+\\d+)?")){
		    	// The ID isn't valid as an in-game ID, and so it does not match regex "number" with optional ": + number"
		    	say("ERROR: That's not a valid Minecraft item ID!");
		    	return;
		    }
		    // Check if the ID isn't taken already
		    Item tempItem = data.findItemByID(iD); 
		    if(tempItem != null){
		    	say("ERROR: ID " + iD + " is already taken by the item \"" + tempItem.getName() + "\"!");
		    	return;
		    }
		    if(name == null || name.equals("")){
		    	say("ERROR: No item name was entered!");
		    	return;
		    }
		    
		    // Get tags out of submitted String in tagsField
		    ArrayList<String> tags = new ArrayList<String>();
		    try {
		    	StringTokenizer tokenizer = new StringTokenizer(potentialTags, ", ;/");
		    	String testToken = null;
		    	while(tokenizer.hasMoreTokens()){
		    		testToken = tokenizer.nextToken();
		    		if(testToken != null && testToken != "") {
		    			tags.add(testToken);
		    		}
		    	}
		    } catch(NoSuchElementException e) {
		    	System.err.println("No Such Element.");
		    	e.printStackTrace();
		    } catch(NullPointerException e) {
		        System.err.println("Does not contain().");
		        e.printStackTrace();
		    }
		    // If all is well, the item may be added.
		    Item newItem = new Item(iD, name, obtainable, tags);
		    data.addItem(newItem);
			itemPanel.clearSearchBar();
			data.sortItems();
			refresh();
		    say("Item \"" + name + "\" added!");
		    itemPanel.setSelected(newItem);
		}
	}
	
	private void menuEditItem(){
		if(selectedItem == null) return;
		
		JTextField iDField = new JTextField(selectedItem.getID());
		JTextField nameField = new JTextField(selectedItem.getName());
		JCheckBoxMenuItem obtainableField = new JCheckBoxMenuItem("", selectedItem.isObtainable());
		JTextField tagsField = new JTextField(selectedItem.getTagsAsString());
		JButton deleteButton = new JButton("Delete Item");
		Object[] options = {
		    "Item ID: ", iDField,
		    "Item name: ", nameField,
		    "Is the item obtainable in Survival mode? ", obtainableField,
		    "Item tags for searching: ", tagsField,
		    deleteButton,
		};
		
		deleteButton.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent event) {
				String name = selectedItem.getName();
				data.removeItem(selectedItem);
				selectedItem = null;
				itemPanel.setSelected(selectedItem);
				JOptionPane.getRootFrame().dispose();
				say("Item \"" + name + "\" successfully removed!");
				itemPanel.clearSearchBar();
				return;
			}
		});
		 
		int option = JOptionPane.showConfirmDialog(null, options, "Edit Item " + selectedItem.getName(), JOptionPane.OK_CANCEL_OPTION);
		if (option == JOptionPane.OK_OPTION) {
		    String iD = iDField.getText();
		    String name = nameField.getText();
		    boolean obtainable = obtainableField.isSelected();
		    String potentialTags = tagsField.getText();
		    
		    // Now make sure all fields are valid
		    if(!iD.matches("\\d+(:+\\d+)?")){
		    	// The ID isn't valid as an in-game ID, and so it does not match regex "number" with optional ": + number"
		    	say("ERROR: That's not a valid Minecraft item ID!");
		    	return;
		    }
		    // Check if the ID isn't taken already other than by itself
		    Item tempItem = data.findItemByID(iD); 
		    if(tempItem != null && !tempItem.equals(selectedItem)){
		    	say("ERROR: ID " + iD + " is already taken by the item \"" + tempItem.getName() + "\"!");
		    	return;
		    }
		    if(name == null || name.equals("")){
		    	say("ERROR: No item name was entered!");
		    	return;
		    }
		    
		    // Get tags out of submitted String in tagsField
		    ArrayList<String> tags = new ArrayList<String>();
		    try {
		    	StringTokenizer tokenizer = new StringTokenizer(potentialTags, ", ;/");
		    	String testToken = null;
		    	while(tokenizer.hasMoreTokens()){
		    		testToken = tokenizer.nextToken();
		    		if(testToken != null && testToken != "") {
		    			tags.add(testToken);
		    		}
		    	}
		    } catch(NoSuchElementException e) {
		    	System.err.println("No Such Element.");
		    	e.printStackTrace();
		    } catch(NullPointerException e) {
		        System.err.println("Does not contain().");
		        e.printStackTrace();
		    }
		    // If all is well, the item may be replaced
		    Item newItem = new Item(iD, name, obtainable, tags);
		    if(selectedItem.equals(newItem)){
		    	say("ERROR: No adjustments made!");
		    } else {
			    data.replaceItem(selectedItem, newItem);
				itemPanel.clearSearchBar();
				data.sortItems();
				refresh();
				say("\"" + name + "\" successfully edited!");
		    }
		}
	}
	
	private void menuAddRecipe(){
		String[] methods = new String[data.getCraftingMethods().size()];
		int i = 0;
		// Get all possible crafting methods
		for(CraftingMethod method : data.getCraftingMethods()){
			methods[i] = method.getName();
			i++;
		}
		
		JComboBox<String> craftingMethodField = new JComboBox<>(methods);
		JTextField inputField = new JTextField();
		JTextField outputField =  new JTextField();
		
		Object[] options = {
				"Crafting Method", craftingMethodField,
				"Input item IDs", inputField,
				"Output item IDs", outputField,
		};
		
		int option = JOptionPane.showConfirmDialog(null, options, "Add Recipe...", JOptionPane.OK_CANCEL_OPTION);
		if(option == JOptionPane.OK_OPTION) {
			if(craftingMethodField.getSelectedItem() == null){
				say("ERROR: no recipe selected!");
				return;
			}
			CraftingMethod craftingMethod = data.findCraftingMethodByName(craftingMethodField.getSelectedItem().toString());
			String inputTokens = inputField.getText();
			String outputTokens = outputField.getText();
			
			ArrayList<Item> input = new ArrayList<>();
			ArrayList<Item> output = new ArrayList<>();
			
			StringTokenizer token = new StringTokenizer(inputTokens, ", ;/");
			String temp = "";
			Item tempItem = null;
			while(token.hasMoreTokens()){
				temp = token.nextToken();
				tempItem = data.findItemByID(temp);
				if(tempItem != null) {
					input.add(tempItem);
				} else {
					say("ERROR: there is no item with ID " + temp + "!");
					return;
				}
			}
			token = new StringTokenizer(outputTokens, ", ;/");
			while(token.hasMoreTokens()){
				temp = token.nextToken();
				tempItem = data.findItemByID(temp);
				if(tempItem != null) {
					output.add(tempItem);
				} else {
					say("ERROR: there is no item with ID " + temp + "!");
					return;
				}
			}
			if(input.isEmpty() || output.isEmpty()){
				say("ERROR: input and output must contain item IDs!");
				return;
			}
			// Add empty spots for lacking input, in case the input does not require all craftingmethod slots
			while(input.size() < craftingMethod.getInputSize()){
				input.add(data.findItemByID("0"));
			}
		    // If all is well, the recipe may be added.
		    data.addRecipe(new Recipe(craftingMethod, input, output));
			itemPanel.clearSearchBar();
			refresh();
			say("Recipe successfully added!");
		}
	}
	
	private void menuEditRecipe(){
		String[] recipeList = new String[data.getRecipes().size()];
		int i = 0;
		// Get all possible recipes in a list
		for(Recipe recipe : data.getRecipes()){
			recipeList[i] = recipe.getOutputNamesAsString() + "   <-   " + recipe.getInputNamesAsString();
			i++;
		}
		
		// Make a combo box with all recipes for selection
		JComboBox<String> recipeSelector = new JComboBox<>(recipeList);
		Object[] selector = {
			"Recipe: ", recipeSelector,
			};
		
		int option = JOptionPane.showConfirmDialog(null, selector, "Edit Recipe...", JOptionPane.OK_CANCEL_OPTION);
		if (option == JOptionPane.OK_OPTION) {
			// Remember the selected recipe
			if(recipeSelector.getSelectedItem() == null){
				say("ERROR: no recipe selected!");
				return;
			}
			Recipe selectedRecipe = data.getRecipes().get(recipeSelector.getSelectedIndex());
			// Set the crafting method as it currently is
			String[] methods = new String[data.getCraftingMethods().size()];
			i = 0;
			for(CraftingMethod method : data.getCraftingMethods()){
				methods[i] = method.getName();
				i++;
			}
			JComboBox<String> craftingMethodField = new JComboBox<>(methods);
			craftingMethodField.setSelectedItem(selectedRecipe.getCraftingMethod().getName());
			
			// Set the input and output as it currently is
			String in = "";
			for(Item item : selectedRecipe.getInput()){
				in += item.getID() + " ";
			}
			// Remove final ' '
			in = in.length() > 0 ? in.substring(0, in.length()-1) : in;
			String out = "";
			for(Item item : selectedRecipe.getOutput()){
				out += item.getID() + " ";
			}
			// Remove final ' '
			out = out.length() > 0 ? out.substring(0, out.length()-1) : out;
			JTextField inputField = new JTextField(in);
			JTextField outputField =  new JTextField(out);
			JButton deleteButton = new JButton("Delete");
			
			Object[] options = {
					"Crafting Method", craftingMethodField,
					"Input item IDs", inputField,
					"Output item IDs", outputField,
					deleteButton,
			};
			
			deleteButton.addActionListener(new ActionListener(){
				@Override
				public void actionPerformed(ActionEvent event) {
					if(selectedRecipe != null){
						data.removeRecipe(selectedRecipe);
						JOptionPane.getRootFrame().dispose();
						say("Recipe successfully removed!");
						itemPanel.clearSearchBar();
						return;
					}
				}
			});
			
			option = JOptionPane.showConfirmDialog(null, options, "Edit Recipe...", JOptionPane.OK_CANCEL_OPTION);
			if(option == JOptionPane.OK_OPTION) {
				CraftingMethod method = data.findCraftingMethodByName(craftingMethodField.getSelectedItem().toString());
				String inputTags = inputField.getText();
				String outputTags = outputField.getText();
				
				ArrayList<Item> input = new ArrayList<>();
				ArrayList<Item> output = new ArrayList<>();
				
				StringTokenizer token = new StringTokenizer(inputTags, ", ;/");
				String temp = "";
				Item tempItem;
				while(token.hasMoreTokens()){
					temp = token.nextToken();
					tempItem = data.findItemByID(temp);
					if(tempItem != null) {
						input.add(tempItem);
					} else {
						say("ERROR: there is no item with ID " + temp + "!");
						return;
					}
				}
				token = new StringTokenizer(outputTags, ", ;/");
				while(token.hasMoreTokens()){
					temp = token.nextToken();
					tempItem = data.findItemByID(temp);
					if(tempItem != null) {
						output.add(tempItem);
					} else {
						say("ERROR: there is no item with ID " + temp + "!");
						return;
					}
				}
				if(input.isEmpty() || output.isEmpty()){
					say("ERROR: input and output must contain item IDs!");
					return;
				}
				// Add empty spots for lacking input, in case the input does not require all craftingmethod slots
				while(input.size() < method.getInputSize()){
					input.add(data.findItemByID("0"));
				}
				// Make sure crafting method conversion is allowed
				if(input.size() != method.getInputSize() || output.size() != method.getOutputSize()){
					say("ERROR: Input or output size does not match the crafting method size!    input: " + input.size() + "/" + method.getInputSize() + ", output: " + output.size() + "/" + method.getOutputSize());
					return;
				}
				
			    // If all is well, the recipe may be replaced.
				Recipe newRecipe = new Recipe(method, input, output);
				if(selectedRecipe.equals(newRecipe)){
					say("ERROR: No adjustments made!");
					return;
				} else {
					data.replaceRecipe(selectedRecipe, newRecipe);
					itemPanel.clearSearchBar();
					refresh();
					say("Recipe successfully edited!");
				}
			}
		}
	}
	
	private void menuAddCrafting(){ 
		JTextField nameField = new JTextField();
		JTextField tagsField = new JTextField();
		JTextField inputWidthField = new JTextField();
		JTextField inputHeightField = new JTextField();
		JTextField outputWidthField = new JTextField();
		JTextField outputHeightField = new JTextField();
		Object[] options = {
		    "Name of the crafting method", nameField,
		    "\nOptional tags that items in this kind of recipe should have (such as \"smeltable\")\nThese items are then required as well for the crafting method\n", tagsField,
		    "\nThe following widths and heights define the\ncrafting method's layout and number of input & output items\n\nWidth of the input grid", inputWidthField,
		    "Height of the input grid", inputHeightField,
		    "\nWidth of the output grid", outputWidthField,
		    "Height of the output grid", outputHeightField
		};
		
		// Set default values for improved user experience
		inputWidthField.setText("3");
		inputHeightField.setText("3");
		outputWidthField.setText("1");
		outputHeightField.setText("1");
		
		int option = JOptionPane.showConfirmDialog(null, options, "Add Crafting Method...", JOptionPane.OK_CANCEL_OPTION);
		if (option == JOptionPane.OK_OPTION) {
		    String name = nameField.getText();
		    String potentialTags = tagsField.getText();
		    int inputWidth = Integer.parseInt(inputWidthField.getText());
		    int inputHeight = Integer.parseInt(inputHeightField.getText());
		    int outputWidth = Integer.parseInt(outputWidthField.getText());
		    int outputHeight = Integer.parseInt(outputHeightField.getText());
		    // Now make sure all fields are valid
		    
		    // Check if the name isn't taken already
		    if(data.findCraftingMethodByName(nameField.getText()) != null){
		    	say("ERROR: crafting method " + name + " is already present!");
		    	return;
		    }
		    if(name == null || name.equals("")){
		    	say("ERROR: no name was entered!");
		    	return;
		    }
		    if(inputWidth <= 0 || inputHeight <= 0 || outputWidth <= 0 || outputHeight <= 0) {
		    	say("ERROR: invalid crafting grid size!");
		    	return;
		    }
		    
		    // Get tags out of submitted String in tagsField
		    ArrayList<String> tags = new ArrayList<String>();
		    try {
		    	StringTokenizer tokenizer = new StringTokenizer(potentialTags, ", ;/");
		    	String testToken = null;
		    	while(tokenizer.hasMoreTokens()){
		    		testToken = tokenizer.nextToken();
		    		if(testToken != null && testToken != "") {
		    			tags.add(testToken);
		    		}
		    	}
		    } catch(NoSuchElementException e) {
		    	System.err.println("No Such Element.");
		    	e.printStackTrace();
		    } catch(NullPointerException e) {
		        System.err.println("Does not contain().");
		        e.printStackTrace();
		    }
		    // If all is well, the item may be added.
		    CraftingMethod newCraftingMethod = new CraftingMethod(name, inputWidth, inputHeight, outputWidth, outputHeight, tags);
		    data.addCraftingMethod(newCraftingMethod);
			itemPanel.clearSearchBar();
			refresh();
			say("Crafting method \"" + name + "\" successfully created!");
		}
	}
	
	private void menuEditCrafting(){
		String[] methodList = new String[data.getCraftingMethods().size()];
		int i = 0;
		// Get all possible crafting methods in a list
		for(CraftingMethod method : data.getCraftingMethods()){
			methodList[i] = method.getName();
			i++;
		}
		
		// Make a combo box with all crafting methods for selection
		JComboBox<String> methodSelector = new JComboBox<>(methodList);
		Object[] selector = {
			"Crafting Method: ", methodSelector,
			};
		
		int option = JOptionPane.showConfirmDialog(null, selector, "Edit Crafting Method...", JOptionPane.OK_CANCEL_OPTION);
		if (option == JOptionPane.OK_OPTION) {
			// Remember the selected crafting method
			if(methodSelector.getSelectedItem() == null){
				say("ERROR: no crafting method selected!");
				return;
			}
			CraftingMethod selectedMethod = data.getCraftingMethods().get(methodSelector.getSelectedIndex());
			
			JTextField nameField = new JTextField();
			JTextField tagsField = new JTextField();
			JTextField inputWidthField = new JTextField();
			JTextField inputHeightField = new JTextField();
			JTextField outputWidthField = new JTextField();
			JTextField outputHeightField = new JTextField();
			JButton deleteButton = new JButton("Delete");
			Object[] options = {
			    "Name of the crafting method", nameField,
			    "\nOptional tags that items in this kind of recipe should have (such as \"smeltable\")\nThese items are then required as well for the crafting method\n", tagsField,
			    "\nThe following widths and heights define the\ncrafting method's layout and number of input & output items\n\nWidth of the input grid", inputWidthField,
			    "Height of the input grid", inputHeightField,
			    "\nWidth of the output grid", outputWidthField,
			    "Height of the output grid", outputHeightField,
			    deleteButton
			};
			
			deleteButton.addActionListener(new ActionListener(){
				@Override
				public void actionPerformed(ActionEvent event) {
					if(selectedMethod != null){
						data.removeCraftingMethod(selectedMethod);
						JOptionPane.getRootFrame().dispose();
						say("Crafting method successfully removed!");
						itemPanel.clearSearchBar();
						return;
					}
				}
			});
			
			// Set values according to previous crafting method
			nameField.setText(selectedMethod.getName());
			tagsField.setText(selectedMethod.getTagsAsString());
			inputWidthField.setText(String.valueOf(selectedMethod.getInputWidth()));
			inputHeightField.setText(String.valueOf(selectedMethod.getInputHeight()));
			outputWidthField.setText(String.valueOf(selectedMethod.getOutputWidth()));
			outputHeightField.setText(String.valueOf(selectedMethod.getOutputHeight()));
			
			option = JOptionPane.showConfirmDialog(null, options, "Edit Crafting Method...", JOptionPane.OK_CANCEL_OPTION);
			if(option == JOptionPane.OK_OPTION) {
				// Make sure all fields are valid
			    
			    // Check if the name isn't taken already
			    if(data.findCraftingMethodByName(nameField.getText()) != null){
			    	say("ERROR: crafting method " + nameField.getText() + " is already present!");
			    	return;
			    }
			    if(nameField.getText() == null || nameField.getText().equals("")){
			    	say("ERROR: no name was entered!");
			    	return;
			    }
			    if(Integer.parseInt(inputWidthField.getText()) <= 0 || Integer.parseInt(inputHeightField.getText()) <= 0 || Integer.parseInt(outputWidthField.getText()) <= 0 || Integer.parseInt(outputHeightField.getText()) <= 0) {
			    	say("ERROR: invalid crafting grid size!");
			    	return;
			    }
			    
			    // Get tags out of submitted String in tagsField
			    ArrayList<String> tags = new ArrayList<String>();
			    try {
			    	StringTokenizer tokenizer = new StringTokenizer(tagsField.getText(), ", ;/");
			    	String testToken = null;
			    	while(tokenizer.hasMoreTokens()){
			    		testToken = tokenizer.nextToken();
			    		if(testToken != null && testToken != "") {
			    			tags.add(testToken);
			    		}
			    	}
			    } catch(NoSuchElementException e) {
			    	System.err.println("No Such Element.");
			    	e.printStackTrace();
			    } catch(NullPointerException e) {
			        System.err.println("Does not contain().");
			        e.printStackTrace();
			    }
			    // If all is well, the item may be added.
			    CraftingMethod newMethod = new CraftingMethod(nameField.getText(), Integer.parseInt(inputWidthField.getText()), Integer.parseInt(inputHeightField.getText()), Integer.parseInt(outputWidthField.getText()), Integer.parseInt(outputHeightField.getText()), tags);
			    if(newMethod.equals(selectedMethod)){
			    	say("ERROR: No adjustments made!");
			    } else {
				    data.replaceCraftingMethod(selectedMethod, newMethod);
					itemPanel.clearSearchBar();
					refresh();
					say("Crafting method successfully edited!");
			    }
			}
		}
	}
	
	private void refresh(){
		itemPanel.revalidate();
		itemPanel.repaint();
		craftingPanel.getItemInfoPanel().revalidate();
		craftingPanel.getItemInfoPanel().repaint();
		craftingPanel.getCraftingInfoPanel().getRecipePanel().revalidate();
		craftingPanel.getCraftingInfoPanel().getRecipePanel().repaint();
		craftingPanel.getCraftingInfoPanel().getRequirementsPanel().revalidate();
		craftingPanel.getCraftingInfoPanel().getRequirementsPanel().repaint();
		craftingPanel.getCraftingInfoPanel().getTagPanel().revalidate();
		craftingPanel.getCraftingInfoPanel().getTagPanel().repaint();
		craftingPanel.getCraftingInfoPanel().revalidate();
		craftingPanel.getCraftingInfoPanel().repaint();
		craftingPanel.revalidate();
		craftingPanel.repaint();
		this.revalidate();
		this.repaint();
		data.sortItems();
		say("That was refreshing!");
	}
	
	private void menuReload(){
		menuNew();
		data.loadFile(filePath);
	}
	
	public void menuToggleObtainable(){
		itemPanel.setSearchBar("");
		if(!data.isShowingOnlyObtainable()){
			itemPanel.setItems(data.getObtainableItems());
			toggleObtainableItem.setText("Include Unobtainable Items");
		} else {
			itemPanel.setItems(data.getItems());
			toggleObtainableItem.setText("Exclude Unobtainable Items");
		}
		data.setShowingOnlyObtainable(!data.isShowingOnlyObtainable());
	}
	
	private void menuTips(){
		JOptionPane.showMessageDialog(null, "Not only can you search for item names, but searching also accepts an item's ID or tags.\n"
				+ "These tags may include alternative names for an item as well as crafting specific variables, such as \"smeltable\".\n\nTry it out for yourself!");
	}
	
	private void menuAbout(){
		JOptionPane.showMessageDialog(null, "This application is built for looking up Minecraft items and recipes.\n\n"
				+ "The supplied file \"minecraftrecipes.tsv\" is based on:\n"
				+ "Minecraft PC version 1.8.6");
	}
	
	public Database getData(){
		return data;
	}
	
	public void enableImages(){
		craftingPanel.enableImages(true);
	}
	
	public void say(String s){
		statusBar.setText(s);
	}
	
	public void clearText(){
		statusBar.setText("");
	}
	
	public void onNotify(String s) {
		// Notified by the Item Panel
		if(s == null) return;
		if(s.equals("")){
			clearText();
		}
		// Notified by the item panel with a selection change: string contains ID of selected item
		clearText();
		selectedItem = data.findItemByID(s);
		if(selectedItem != null){
			nextSequence.push(selectedItem);
			craftingPanel.findImageWithID(s);
			if(selectedItem.isCraftable()) {
				craftingPanel.update(selectedItem, selectedItem.getRecipes().get(0), selectedItem.getRecipes().get(0).getCraftingMethod(), 1);
			} else {
				craftingPanel.update(selectedItem, null, null, 1);
			}
			refresh();
		} 
	}
	
	public void onNotify(CraftingMethod craftingMethod, String s) {
		// Notified by the RequirementsPanel
		ArrayList<Item> items = new ArrayList<>();
		for(Item item : data.getItems()){
			for(String tag : item.getTags()) {
				if(tag.contains(s)){
					items.add(item);
				}
			}
		}
		craftingPanel.showAllItemsWithTag(craftingMethod, s, items);
		refresh();
	}
	
	public void onNotify(Item item, int index){
		//Notified by RecipePanel: change the recipe
		if(!( item != null && index >= 0 && index < item.getRecipes().size() )){
			index = 0;
			// something is wrong, pick the first available (default) recipe.
		}
		craftingPanel.update(item, item.getRecipes().get(index), item.getRecipes().get(index).getCraftingMethod(), 1);

		refresh();
	}
	
	@Override
	public void update(Observable o, Object arg) {
		// Observed an update in the database
		if(data.isShowingOnlyObtainable()){
			itemPanel.onItemChange(data.getObtainableItems());
		} else {
			itemPanel.onItemChange(data.getItems());
		}
		if(arg != null){
			// In this program, Object arg will now be a String object
			say(arg.toString());
		}
		refresh();
	}
	
}
