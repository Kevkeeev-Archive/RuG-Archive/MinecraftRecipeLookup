import java.io.File;

/** @author Kevin Nauta & Herman Groenbroek */

public class Application {
		
	public static void main(String[] arg) {
		String defaultFilePath = "minecraftrecipes.tsv";
		
		AppFrame frame = new AppFrame();
		
		if(new File(defaultFilePath).isFile()){
			frame.setFilePath(defaultFilePath);
			frame.getData().loadFile(defaultFilePath);
			frame.menuToggleObtainable();
		} else {
			frame.menuNew();
		}
		if(new File("itemicons/").isDirectory()){
			frame.enableImages();
		}
		frame.clearText();
	}

}