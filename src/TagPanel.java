import java.awt.BorderLayout;
import java.awt.Dimension;
import java.util.ArrayList;

import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;

@SuppressWarnings("serial")
public class TagPanel extends JPanel{
	
	private JList<String> itemList;
	private JScrollPane pane;
	private int width, height;
	
	public TagPanel(int width, int height) {
		this.width = width;
		this.height = height;
		setPreferredSize(new Dimension(width, height));		
		setVisible(true);
	}
	
	public void update(String tag, ArrayList<Item> items){
		if(itemList != null){
			remove(itemList);
		}
		
		ArrayList<String> itemNames = new ArrayList<>();
		for(Item item : items) {
			for(String itemTag : item.getTags()) {
				if(itemTag.contains(tag)) {
					itemNames.add("<html>" + item.getID() + " - " + item.getName() + " &#8594 " + itemTag + "</html>");
				}
			}
		}
		
		String[] itemArray = new String[itemNames.size()];
		itemArray = itemNames.toArray(itemArray);
		
		itemList = new JList<String>(itemArray);
		itemList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		itemList.setLayoutOrientation(JList.VERTICAL);
		createScrollPane(itemList);
	}
	
	private void createScrollPane(JList<String> list){
		if(pane != null){
			remove(pane);
		}
		pane = new JScrollPane(itemList);
		pane.setViewportView(itemList);
		pane.setPreferredSize(new Dimension(width-20, height));
		add(pane, BorderLayout.WEST);
		revalidate();
	}
	
}