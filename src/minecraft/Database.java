import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.NoSuchElementException;
import java.util.Observable;
import java.util.StringTokenizer;

public class Database extends Observable {

	private ArrayList<Item> items;
	private ArrayList<Recipe> recipes;
	private ArrayList<CraftingMethod> craftingMethods;
	private boolean showingOnlyObtainable;
	
	/**
	 * The constructor of the database, which initializes the arraylists and sets the boolean showingOnlyObtainable to false, by default.
	 * This boolean is used to filter out all items that are unable to be obtained in-game at all, for example: creative items.
	 */
	public Database(){
		items = new ArrayList<>();
		recipes = new ArrayList<>();
		craftingMethods = new ArrayList<>();
		showingOnlyObtainable = false;
	}
	
	public boolean isShowingOnlyObtainable(){
		return showingOnlyObtainable;
	}
	
	public void setShowingOnlyObtainable(boolean value){
		showingOnlyObtainable = value;
	}
	
	public ArrayList<Item> getItems(){
		return items;
	}
	
	public ArrayList<Item> getObtainableItems(){
		ArrayList<Item> obtainableItems = new ArrayList<>();
		for(Item item : items){
			if(item.isObtainable()) obtainableItems.add(item);
		}
		return obtainableItems;
	}
	
	public ArrayList<Recipe> getRecipes(){
		return recipes;
	}
	
	public ArrayList<CraftingMethod> getCraftingMethods(){
		return craftingMethods;
	}
	
	/**
	 * This will add an item to the database, and sort the list afterwards.
	 * @param item The item to be added.
	 * @see Item
	 */
	public void addItem(Item item){
		items.add(item);
		sortItems();
		setChanged();
		notifyObservers();
	}
	
	/**
	 * When an item has been edited, this function is called to compare the old and new item.
	 * If they are different, then the new items information will be added to the list, and afterwards the list will be sorted.
	 * @param old The item as it was before editing.
	 * @param edited The item after the editing has been done.
	 * @see Item
	 */
	public void replaceItem(Item old, Item edited){
		for(Item change : items){
			if(old.equals(change)){
				// Change item values
				change.setName(edited.getName());
				change.setObtainable(edited.isObtainable());
				change.setTags(edited.getTags());
				// Change affected recipes
				if(!old.getID().equals(edited.getID())){
					change.setID(edited.getID());
					for(Recipe recipe : recipes){
						if(recipe.getInput().contains(old)){
							recipe.getInput().set(recipe.getInput().indexOf(old), change);
						}
						if(recipe.getOutput().contains(old)){
							recipe.getOutput().set(recipe.getOutput().indexOf(old), change);
						}
					}
				}
			}
		}
		sortItems();
		setChanged();
		notifyObservers();
	}
	
	/**
	 * If an item has to be removed, this method is executed.
	 * It will first remove the item from the itemlist, and afterwards it will remove all recipes that contain said item.
	 * @param item The item to be removed.
	 * @see Item
	 */
	public void removeItem(Item item){
		// Remove an item and all recipes that contain this item
		items.remove(item);
		ArrayList<Recipe> deleteList = new ArrayList<>();
		for(Recipe recipe : recipes){
			if(recipe.getInput().contains(item) || recipe.getOutput().contains(item)){
				deleteList.add(recipe);
			}
		}
		for(Recipe recipe : deleteList){
			removeRecipe(recipe);
		}
		sortItems();
		setChanged();
		notifyObservers();
	}
	
	/**
	 * This will add a recipe to the database, and also add the recipe to all unique items in its output-list.
	 * @param recipe The recipe to be added.
	 * @see Recipe
	 */
	public void addRecipe(Recipe recipe){
		ArrayList<Item> tempList = new ArrayList<>();
		for(Item testItem : recipe.getOutput()){
			if(! tempList.contains(testItem)){
				tempList.add(testItem);
			}
		} // get the number of unique items in the output array
		
		for(Item item : tempList){
			item.getRecipes().add(recipe);
		}
		
		recipes.add(recipe);
		setChanged();
		notifyObservers();
	}
	
	/**
	 * When a recipe is edited, this method is called to compare the two recipes.
	 * If they are different, then all information from the edited recipe will be implemented in the database, and all items containing the old recipe will now get the new recipe.
	 * @param old The recipe before it had been edited.
	 * @param edited The recipe after it had been edited.
	 * @see Recipe
	 */
	public void replaceRecipe(Recipe old, Recipe edited){
		ArrayList<Recipe> deleteList = new ArrayList<>();
		for(Item item : items){
			if(item.getRecipes().contains(old)){
				deleteList.add(old);
				item.getRecipes().add(edited);
			}
		}
		for(Recipe recipe : deleteList) {
			recipes.remove(recipe);
		}
		
		recipes.remove(old);
		recipes.add(edited);
		setChanged();
		notifyObservers();
	}
	
	/**
	 * This method will remove a recipe from the database, and also remove it from all items that are created with this recipe.
	 * @param recipe The recipe to be removed.
	 * @see Recipe
	 */
	public void removeRecipe(Recipe recipe){
		// Remove all occurrences of the recipe in an output item's recipelist, then remove the recipe itself
		for(Item item : items){
			if(item.getRecipes().contains(recipe)){
				item.getRecipes().remove(recipe);
			}
		}
		recipes.remove(recipe);
		setChanged();
		notifyObservers();
	}
	
	/**
	 * This method will add a crafting method to the database.
	 * @param method The crafting method to be added.
	 * @see CraftingMethod
	 */
	public void addCraftingMethod(CraftingMethod method){
		craftingMethods.add(method);
		setChanged();
		notifyObservers();
	}
	
	/**
	 * When a crafting method has been edited, this method is called to compare the two.
	 * If an edit has been done, then the information from the edited method will be implemented in the database, and all recipes created with this method are adapted to suit the new requirements.
	 * @param old The method before it had been edited.
	 * @param edit The method after it had been edited.
	 * @see CraftingMethod
	 */
	public void replaceCraftingMethod(CraftingMethod old, CraftingMethod edit){
		// Replace recipes containing the crafting method, then the method itself
		Recipe editRecipe;
		for(Recipe oldRecipe : recipes){
			editRecipe = oldRecipe;
			if(editRecipe.getCraftingMethod().getName().equals(old.getName())){
				
				if(old.getInputSize() < edit.getInputSize()){
					for(int i = 0; i < (edit.getInputSize() - old.getInputSize()); i++) {
						editRecipe.getInput().add(findItemByID("0"));
					} // inputsize got bigger, add zeroes.
				} else if(old.getInputSize() > edit.getInputSize()) {
					ArrayList<Item> newInput = new ArrayList<>();
					for(int i = 0; i < edit.getInputSize(); i++){
						newInput.add(editRecipe.getInput().get(i));
					} // inputsize got smaller, remove excess items
					editRecipe.setInput(newInput);
				}
				
				if(old.getOutputSize() < edit.getOutputSize()){
					for(int i = 0; i < (edit.getOutputSize() - old.getOutputSize()); i++) {
						editRecipe.getOutput().add(findItemByID("0"));
					} // outputsize got bigger, add zeroes.
				} else if(old.getOutputSize() > edit.getOutputSize()) {
					ArrayList<Item> newOutput = new ArrayList<>();
					for(int i = 0; i < edit.getOutputSize(); i++){
						newOutput.add(editRecipe.getOutput().get(i));
					} // outputsize got smaller, remove excess items
					editRecipe.setInput(newOutput);
				}
								
				editRecipe.setCraftingMethod(edit);
				replaceRecipe(oldRecipe, editRecipe);
			}
		}
		craftingMethods.remove(craftingMethods.indexOf(old));
		craftingMethods.add(edit);
		
		setChanged();
		notifyObservers();
	}
	
	/**
	 * This method will remove the specified crafting method from the database, and also remove all recipes created with this method.
	 * @param method The crafting method to be removed.
	 * @see CraftingMethod
	 */
	public void removeCraftingMethod(CraftingMethod method){
		// Remove recipes containing the crafting method, then the method itself
		ArrayList<Recipe> deleteList = new ArrayList<>();
		for(Recipe recipe : recipes){
			if(recipe.getCraftingMethod().getName().equals(method.getName())){
				deleteList.add(recipe);
			}
		}
		for(Recipe recipe : deleteList){
			removeRecipe(recipe);
		}
		craftingMethods.remove(method);
		setChanged();
		notifyObservers();
	}
	
	public void sortItems(){
		Collections.sort(items);
		setChanged();
		notifyObservers();
	}
	
	/**
	 * This method will search for any item that contains the given ID, and return that item. If no item is found, it will return null.
	 * @param iD The ID of an item to be found
	 * @return The item with the given ID, or null if no item is found.
	 * @see Item
	 */
	public Item findItemByID(String iD){
		for(Item item : items){
			if(iD.equals(item.getID())) {
				return item;
			}
		}
		// No item exists with the given ID
		return null;
	}
	
	/**
	 * This will search for any crafting method that has the specified name, and return that method. If no method is found, it will return null.
	 * @param name The specified name.
	 * @return The crafting Method with the specified name, or null if no method is found.
	 * @see CraftingMethod
	 */
	public CraftingMethod findCraftingMethodByName(String name){
		for(CraftingMethod method : craftingMethods){
			if(name.equals(method.getName())) return method;
		}
		// No crafting method exists with the given name
		return null;
	}
	
	/**
	 * If the user wants to add a new Item, this method will look for the fist free ID and return that.
	 * This way, the user does not have to look himself for a free ID.
	 * @return The ID next in line
	 * @see Item
	 */
	public String getNextAvailableID(){
		int tempID = 0;
		for(Item item : items) {
			// Make sure to loop only over items without metadata (data with a ":")
			if(!item.getID().contains(":")){
				if(tempID == Integer.parseInt(item.getID())) {
					tempID += 1;
				} else {
					return Integer.toString(tempID);
				}
			}
		}
		return Integer.toString(tempID);
	}
	
	/**
	 * Clears the database, so a new project can be started or an existing project can be loaded.
	 */
	public void clearData(){
		items.clear();
		recipes.clear();
		craftingMethods.clear();
		setChanged();
		notifyObservers("Data cleared!");
	}
	
	/**
	 * Loads a file containing items, crafting methods and recipes
	 * @param path The location of the file to be loaded
	 */
	public void loadFile(String path){
		StringTokenizer token = null;
		BufferedReader input = null;
		
		try {
			input = new BufferedReader(new FileReader(path));
		} catch (FileNotFoundException e) {
			System.err.println("FILE NOT FOUND! Please check if you supplied the right file!");
			e.printStackTrace();
			return;
		}
		
		int numItems = 0, numRecipes = 0, numMethods = 0;
		
		// Item declarations
		String iD, name, line = null;
		boolean isObtainable;
		ArrayList<String> tags = new ArrayList<>();
		
		// CraftingMethod declarations
		String craftingName;
		String description;
		ArrayList<String> tag = new ArrayList<>();
		int inputWidth, inputHeight, outputWidth, outputHeight;
		
		// Recipe declarations
		String recipeCraftingMethod;
		ArrayList<Item> recipeInput = new ArrayList<>();
		ArrayList<Item> recipeOutput = new ArrayList<>();
		
		try {
			// Reading the items
			input.readLine(); // Skip first 'comment' line
			line = input.readLine();
			token = new StringTokenizer(line);
			while(!line.startsWith("<crafting method definition>")){
				token = new StringTokenizer(line);
				iD = token.nextToken("\t");
				name = token.nextToken();
				isObtainable = Boolean.parseBoolean(token.nextToken("\t"));
				while(token.hasMoreTokens()){
					tags.add(token.nextToken());
				}
				items.add(new Item(iD, name, isObtainable, tags));
				tags = new ArrayList<>();
				numItems++;
				line = input.readLine();
			}
			notifyObservers("Number of items loaded: " + numItems);
			
			// Reading the crafting methods
			line = input.readLine();
			while(line != null && !line.startsWith("<")){
				token = new StringTokenizer(line);
				craftingName = token.nextToken("\t");
				inputWidth = Integer.parseInt(token.nextToken());
				inputHeight = Integer.parseInt(token.nextToken());
				outputWidth = Integer.parseInt(token.nextToken());
				outputHeight = Integer.parseInt(token.nextToken());
				description = token.nextToken();
				while(token.hasMoreTokens()){
					tag.add(token.nextToken());
				}
				craftingMethods.add(new CraftingMethod(craftingName, inputWidth, inputHeight, outputWidth, outputHeight, description, tag));
				numMethods++;
				line = input.readLine();
			}
			notifyObservers("Number of crafting methods loaded: " + numMethods);
			
			// Reading the recipes
			line = input.readLine();
			while(line != null){
				token = new StringTokenizer(line);
				recipeCraftingMethod = token.nextToken("\t");
				String testToken = token.nextToken();
				// Skip "<input:>" token
				testToken = token.nextToken();
				while(!testToken.equals("<output:>")){
					recipeInput.add(findItemByID(testToken));
					testToken = token.nextToken();
				}
				while(token.hasMoreTokens()){
					recipeOutput.add(findItemByID(token.nextToken()));
				}
				recipes.add(new Recipe(findCraftingMethodByName(recipeCraftingMethod), recipeInput, recipeOutput));
				recipeInput = new ArrayList<>();
				recipeOutput = new ArrayList<>();
				numRecipes++;
				line = input.readLine();
			}
			notifyObservers("Number of recipes loaded: " + numRecipes);
			

			
		} catch (NoSuchElementException e) {
			System.err.println("NO MORE TOKENS! Tried to add nonexistent tokens to list from the following line:\n\"" + line + "\"\nPlease check input file.");
			for(Item item : recipeInput){
				System.err.println(item.getID() + " - " + item.getName());
			} for(Item item : recipeOutput) {
				System.err.println(item.getID() + " - " + item.getName());
			}
			e.printStackTrace();
		} catch (IOException e) {
			System.err.println("Something went wrong probably with \"input.readLine()\". See stacktrace for more details:");
			e.printStackTrace();
			System.exit(-3);
		}
		
		// Every item has an arraylist of recipes which have the item as the output - these arrays will now be filled with the correct recipes for quick retrieval
		Item item;
		for(Recipe recipe : recipes){
			for(Item output : recipe.getOutput()){
				item = findItemByID(output.getID());
				if(item != null){
					if(!item.getRecipes().contains(recipe)){
						item.getRecipes().add(recipe);
					}
				} else {
					System.err.println("Recipe " + recipes.indexOf(recipe)+1 + " expects non-existent item with ID " + output.getID() + "!");
				}
			}
		}
		setChanged();
		notifyObservers("File loaded!");
	}
	
	/**
	 * Saves a file containing items, crafting methods and recipes
	 * @param path The location where the file should be saved to.
	 */
	public void saveFile(String path){
		BufferedWriter output = null;
		
		// Fix file extension if necessary, so that every file is saved neatly as .tsv (Tab-Separated Values)
		if(!path.endsWith(".tsv")){
			path += ".tsv";
		}
		
		try {
			output = new BufferedWriter(new FileWriter(path));
		} catch (IOException e) {
			System.err.println("Something went wrong. Perhaps there already is a file '" + path + "'. See stacktrace for more details:");
			e.printStackTrace();
		}
		
		try {
			
			// Info line about items			
			output.write("<ID>\t<name>\t<isObtainable>\t<tags>");
			
			for(Item item : items) {
				output.newLine();
				output.write(item.toString());
			}
			
			// Info line about crafting methods
			output.newLine();
			output.write("<crafting method definition>");
			
			for(CraftingMethod method : craftingMethods){
				output.newLine();
				output.write(method.toString());
			}
						
			// Info line about recipes
			output.newLine();
			output.write("<crafting method>\t<input item ID>\t<output item IDs>");
			
			for(Recipe recipe : recipes) {
				output.newLine();
				output.write(recipe.toString());
			}
			
			output.close();
			
		} catch(IOException e){
			System.err.println("Something went wrong with the output file. See stacktrace for more details:"); 
			e.printStackTrace();
		}
		notifyObservers("File saved!");
	}
	
}
