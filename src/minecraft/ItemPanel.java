import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.util.ArrayList;

import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

@SuppressWarnings("serial")
public class ItemPanel extends JPanel {
	
	private AppFrame listener;
	private ArrayList<Item> items;
	private String[] itemNames;
	private JList<String> itemList;
	private JScrollPane pane;
	private JTextField searchBar;
	private int width;
	private int height;
	
	/**
	 * This is the constructor of the ItemPanel. This panel shows a search bar and a list of items.
	 * The search bar can be used to search for (partial) item names and tags. 
	 * On every keystroke, the list is updated to match the input of the search bar.
	 * @param items The list of items from the database.
	 * @param width The preferred width of the panel.
	 * @param height The preferred height of the panel.
	 */
	public ItemPanel(ArrayList<Item> items, int width, int height){
		this.width = width;
		this.height = height;
		this.items = items;
		setPreferredSize(new Dimension(width, height));
		onItemChange(items);
		createSearchBar();
		createItemList(itemNames);
	}
	
	public JList<String> getItemList(){
		return itemList;
	}
	
	public JTextField getSearchBar(){
		return searchBar;
	}
	
	public void setSearchBar(String s){
		searchBar.setText(s);
	}
	
	public void clearSearchBar(){
		searchBar.setText("");
	}
	
	public void setItems(ArrayList<Item> items){
		onItemChange(items);
	}
	
	
	public Item getSelectedItem(){
		return (itemList.getSelectedIndex() != -1 ? items.get(itemList.getSelectedIndex()) : null); 
	}
	
	public boolean hasSelection(){
		return !(itemList.getSelectedIndex() == -1);
	}
	
	public void setSelected(Item select){
		int i = 0;
		for(Item item : items){
			if(item.equals(select)){
				itemList.setSelectedIndex(i);
				itemList.ensureIndexIsVisible(i);
				return;
			}
			i++;
		}
	}
	
	public void deselect(){
		itemList.setSelectedIndex(-1);
	}
	
	/**
	 * When an item is selected, this method is called.
	 * It sometimes happens that the program does not load correctly and as such the selected index will be -1.
	 * In this case, an error is printed.
	 * Otherwise this will return a string reperesentation of the ID of the selected item.
	 * @see AppFrame
	 */
	private void onSelectionChange(){
		if(itemList.getSelectedIndex() != -1){
			String iD = itemList.getSelectedValue().split(" - ")[0];
			Item item = findItemByID(iD);
			if(item != null){
				// Selected item: item.getName()
				notifyListeners(iD);
			}
		} else {
			System.err.println("Selected index is -1\nThe program failed to initialize correctly!");
			itemList.setSelectedIndex(0);
		}
	}
	
	/**
	 * If the searchbar is used, this method will create a scrollpane of the provided array of names.
	 * @param itemNames The array of item names to be displayed in the scrollpane.
	 */
	private void createItemList(String[] itemNames){
		if(itemList != null){
			remove(itemList);
		}
		itemList = new JList<String>(itemNames);
		itemList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		itemList.setLayoutOrientation(JList.VERTICAL);
		// Add selection listener
		itemList.addListSelectionListener(new ListSelectionListener() {
			public void valueChanged(ListSelectionEvent e) {
				if(e.getValueIsAdjusting()){
					return;
				}
				// List selection has changed
				onSelectionChange();
			}
		});
		createScrollPane(itemList);
	}
	
	/**
	 * When the searchbar is used, this method is called to create the scroll pane around the list of item  names.
	 * @param list The list as created by the method createItemList(String[] itemNames).
	 */
	private void createScrollPane(JList<String> list){
		if(pane != null){
			remove(pane);
		}
		pane = new JScrollPane(itemList);
		pane.setViewportView(itemList);
		pane.setPreferredSize(new Dimension(width, height));
		add(pane, BorderLayout.WEST);
		revalidate();
	}
	
	/**
	 * This method constructs the search bar, which can be used to search for items using a (partial) name or a tag.
	 */
	private void createSearchBar(){
		searchBar = new JTextField("Search...");
		// Make the search bar show a default watermark
		searchBar.addFocusListener(new FocusListener() {
		    public void focusGained(FocusEvent e) {
		    	// Clear watermark before typing
		    	if(searchBar.getText().equals("Search...")){
		    		searchBar.setText("");
		    	}
		    }

		    public void focusLost(FocusEvent e) {
		    	if(searchBar.getText().equals("")){
		    		searchBar.setText("Search...");
		    		// Clear statusbar text
		    		notifyListeners("");
		    	}
		    }
		});
		add(searchBar, BorderLayout.NORTH);
		searchBar.setPreferredSize(new Dimension(width, 20));
		// Make the search bar update results as the user types
		searchBar.getDocument().addDocumentListener(new DocumentListener() {
			public void changedUpdate(DocumentEvent e) {
				search();
			}
			public void removeUpdate(DocumentEvent e) {
				search();
			}
			public void insertUpdate(DocumentEvent e) {
				search();
			}
			public void search() {
				if(searchBar.getText() == null || searchBar.getText().equals("")){
					// Reset item list to the full, original item list
					refreshItemList();
				} else if(!searchBar.getText().contains("Search...")) {
					// Update list with search query
					int i = 0;
					for(Item item : items){
						if(item.getName().toLowerCase().contains(searchBar.getText().toLowerCase()) 
								|| item.getTagsAsString().toLowerCase().contains(searchBar.getText().toLowerCase()) 
								|| item.getID().contains(searchBar.getText())
							){
							i++;
						}
					}
					String[] newNames = new String[i];
					i = 0;
					for(Item item : items){
						if(item.getName().toLowerCase().contains(searchBar.getText().toLowerCase()) 
								|| item.getTagsAsString().toLowerCase().contains(searchBar.getText().toLowerCase()) 
								|| item.getID().contains(searchBar.getText())
							){
							newNames[i] = item.getID() + " - " + item.getName();
							i++;
						}
					}
					createItemList(newNames);
				}
			}
		});
	}
	
	public void refreshItemList(){
		// reset item names
		itemNames = new String[items.size()];
		int i = 0;
		for(Item item : items){
			itemNames[i] = item.getID() + " - " + item.getName();
			i++;
		}
		createItemList(itemNames);
	}
	
	public Item findItemByID(String iD){
		for(Item item : items){
			if(iD.equals(item.getID())){
				return item;
			}
		}
		// No item exists with the given ID
		return null;
	}

	public int partialItemNameToIndex(String s) {
		int index = 0;
		for(Item item : items){
			if(item.getName().toLowerCase().contains(s.toLowerCase())){
				return index;
			} else {
				index++;
			}
		}
		return -1;
	}
	
	/**
	 * When the searchbar is used to look for items, this method will be called.
	 * This method creates a list of item names using the provided ArrayList of items.
	 * @param items The list of items that match whatever the user searches for.
	 */
	public void onItemChange(ArrayList<Item> items){
		this.items = items;
		itemNames = new String[items.size()];
		for(int i=0; i<items.size(); i++){
			itemNames[i] = items.get(i).getID() + " - " + items.get(i).getName();
		}
		createItemList(itemNames);
	}

	public void addListener(AppFrame listener) {
		this.listener = listener;
	}
	
	private void notifyListeners(String s){
		listener.onNotify(s);
	}
	
}
